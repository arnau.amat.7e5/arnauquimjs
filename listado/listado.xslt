<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<body>
				<xsl:apply-templates/>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="article/header">
		<h3>
			<a href="{h3/a/@href}">
				<xsl:value-of select="h3/a"/>
			</a>
		</h3>
	</xsl:template>

	<xsl:template match="article/p">
		<p>
			<xsl:value-of select="."/>
		</p>
	</xsl:template>

	<xsl:template match="article/p[@class='name']">
		<p class="name">
			<b>Dificultad: </b>
			<xsl:value-of select="."/>
		</p>
	</xsl:template>

	<xsl:template match="article/p[@class='receta_detalle_icons']">
		<p class="receta_detalle_icons">
			<xsl:for-each select="img">
				<img class="icon" src="{@src}" alt="{@alt}"/>
			</xsl:for-each>
		</p>
	</xsl:template>

	<xsl:template match="article/p[@class='name'][2]">
		<p class="name">
			<b>Autor: </b>
			<xsl:value-of select="."/>
		</p>
	</xsl:template>

</xsl:stylesheet>
