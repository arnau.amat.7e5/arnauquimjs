"""
script.py
Quim Delgado
Utilitza la llibreria xml.etree per llegir l'xml i substituir els elements per elements de diferents llistes.
Ho fa 30 cops i escriu un nou fitxer. Agafa l'estructura de l'arxiu estructura.xml
"""

import random
import xml.etree.ElementTree as ET

ingredientes = ['aceite de oliva', 'aceitunas', 'ajo', 'albahaca', 'alcaparras', 'almendras', 
                'anchoas', 'apio', 'arroz', 'atún', 'avena', 'azúcar', 'bacalao', 'berenjena', 
                'calabacín', 'calamares', 'camarones', 'cebolla', 'cebolla de verdeo', 
                'cebolla morada', 'cereales', 'cerdo', 'champiñones', 'chile', 'chocolate',
                'cilantro', 'ciruelas', 'clara de huevo', 'cocido', 'coliflor', 'comino', 
                'crema de leche', 'cuajada', 'curry', 'dátiles', 'eneldo', 'espinacas', 
                'fideos', 'flor de sal', 'garbanzos', 'garam masala', 'gelatina', 'gengibre',
                'guisantes', 'habas', 'harina', 'hierbabuena', 'hinojo', 'huevos', 'jamón', 
                'jengibre', 'judías verdes', 'kétchup', 'laurel', 'lentejas', 'lima', 'limón', 
                'lomo', 'macarrones', 'maíz', 'manzana', 'mantequilla', 'mayonesa', 'mejillones',
                'merluza', 'miel', 'mostaza', 'nata', 'nueces', 'orégano', 'palmitos', 'pan',
                'panceta', 'papas', 'pasta', 'patatas', 'pavo', 'perejil', 'peras', 'pescado', 
                'pimienta', 'pimiento', 'pimiento rojo', 'pimiento verde', 'pollo', 'queso', 
                'rabanitos', 'romero', 'sal', 'salchichas', 'salmón', 'semillas de sésamo', 'soja', 
                'sopa', 'tarta', 'té', 'tomate', 'tomillo', 'trigo', 'vainilla', 'vino', 'yogur', 
                'zanahorias', 'zarzamoras', 'zumo']
dificultad = ["alta", "media", "baja"]
cocina = ["italiana", "mexicana", "francesa", "japonesa", "mediterránea"]
bool = ["si", "no"]

def generar_receta(ingredientes):
    with open(r"D:\ASIXc\llenguatge_de_marques\Pt3\Pt3.3\listado\estructura.xml", "r", encoding="utf-8") as file:
        tree = ET.parse(file)
        root = tree.getroot()

    article = root.find("article")
    new_article = ET.Element("article")
    new_article.extend(article)

    ingredientes = random.sample(ingredientes, 2)
    h3_a = new_article.find("header").find("h3").find("a")
    h3_a.text = " ".join(ingredientes)

    p = new_article.find("p")
    p.text = f"Receta de {ingredientes[0]} {ingredientes[1]}"

    name = new_article.find("p[@class='name']")
    name_elements = name.findall("b")
    name_elements[1].tail = random.choice(dificultad)
    name_elements[3].tail = random.choice(cocina)
    name_elements[4].tail = random.choice(bool)

    with open(r"D:\ASIXc\llenguatge_de_marques\Pt3\Pt3.3\listado\listado.xml", "a", encoding="utf-8") as file:
        if file.tell() == 0:
            file.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            file.write('<articles>\n')

        file.write("\t")
        file.write(ET.tostring(new_article, encoding="unicode"))
        file.write("\n")

for _ in range(30):
    generar_receta(ingredientes)

with open(r"D:\ASIXc\llenguatge_de_marques\Pt3\Pt3.3\listado\listado.xml", "a", encoding="utf-8") as file:
    file.write('</articles>\n')

